![The Prodgomin Banner](/assets/prodgomin_banner.png)

# Prodgomin
Prodgomin is a production minifier written in Go, its goal is to create a minified version of your codebase directly in your production environment. Its use case is limited, mainly for big apps that don't require a build step (e.g. enterprise PHP applications)

## Building
It may not neccessary to build Prodgomin yourself, check the [Releases](https://gitlab.com/jancraft888/prodgomin/-/releases) section for downloads.


To build Prodgomin, simply run:
```sh
go build
```

## Usage
Prodgomin is very powerful and very configurable, as such, I recommend you run `prodgomin --help` to get the latest features.


The `prodgomin <input files>` command is the main command, it runs Prodgomin with all the CLI config you could ask for!

| Flag        | Short | Notes                                                    |
|-------------|-------|----------------------------------------------------------|
| --all       | -a    | Minifies ALL source files                                |
| --js        |       | Minifies JS source files                                 |
| --css       |       | Minifies CSS source files                                |
| --html      |       | Minifies HTML source files                               |
| --php       |       | Minifies PHP source files                                |
| --overwrite | -o    | Overwrites the source files                              |
| --extension | -e    | Defines new custom extensions for a format, 'ext:format' |
| --gz        |       | Compresses the output files using GZip                   |
| --br        |       | Compresses the output files using Brotli                 |

Additionally, you can configure the HTML minifier to suit your needs.

| Flag                           |
|--------------------------------|
| --html-KeepConditionalComments |
| --html-KeepEndTags             |
| --html-KeepQuotes              |
| --html-KeepWhitespace          |

## Config file
While Prodgomin can be used without config files, bigger projects may benefit from an organized Config File.

To run Prodgomin from a config file use:
```sh
prodgomin config <config file>
```
Keep in mind all paths on the config file are relative to the current working directory and not the config file itself.

```yaml
---
paths:           # runs the minifier in these glob(s)
  - ./*
formats:         # which formats to minify, "all" is valid here
  - js
compress:        # compress the output
  - gzip
  - brotli
---
# when omitting paths, it reuses the last paths that was explicitlly defined
formats:
  - css
  - html
extensions:      # defines a custom extension as a certain format
  - tpl:html
html:            # html minifier config
  - KeepQuotes
---
formats:
  - php
overwrite: false # for PHP in Nginx/Apache, this probably should be true (only in prod)
---
```
