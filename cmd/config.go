/*
Copyright © 2022 Jan Garcia

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"fmt"
        "gopkg.in/yaml.v3"
        "os"
        "io"
        "errors"

        "path/filepath"

	"github.com/spf13/cobra"
)

type Setting struct {
  Paths []string `yaml:"paths"`
  Formats []string `yaml:"formats"`
  Compress []string `yaml:"compress"`
  Extensions []string `yaml:"extensions"`
  Overwrite bool `yaml:"overwrite"`
  HtmlMinOpts []string `yaml:"html"`
}

var lastpath []string

func process(cmd *cobra.Command, spec *Setting) {
  var paths []string
  for _, v := range spec.Paths {
    arr, err := filepath.Glob(v)
    if err != nil { panic(err) }
    for _, a := range arr {
      paths = append(paths, a)
    }
  }
  if len(paths) == 0 { paths = lastpath }
  lastpath = paths

  args := []string{}
  if spec.Overwrite { args = append(args, "--overwrite") }
  for _, v := range spec.Formats {
    args = append(args, "--" + v)
  }
  for _, v := range spec.Compress {
    if v == "gzip" { v = "gz" }
    if v == "brotli" { v = "br" }
    args = append(args, "--" + v)
  }
  for _, v := range spec.Extensions {
    args = append(args, "-e " + v)
  }
  for _, v := range spec.HtmlMinOpts {
    args = append(args, "--html-" + v)
  }
  fmt.Printf("prodgomin ")
  for _, v := range paths { args = append(args, v) }
  for _, v := range args { fmt.Printf("%s ", v) }
  fmt.Println()
  rootCmd.ResetFlags()
  RootCommandInit()
  rootCmd.SetArgs(args)
  rootCmd.Execute()
}

// configCmd represents the config command
var configCmd = &cobra.Command{
	Use:   "config <file>",
	Short: "Runs Prodgomin using the provided config",
	Long: `Runs Prodgomin using the provided config, it allows you to run multiple settings for different files and allows an easier to read configuration format.`,
        Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
          var sets []*Setting
          f, err := os.Open(args[0])
          if err != nil {
            panic(err)
          }
          d := yaml.NewDecoder(f)
          for {
            spec := new(Setting)
            err := d.Decode(&spec)
            if spec == nil {
              continue
            }
            if errors.Is(err, io.EOF) {
              break
            }
            if err != nil {
              panic(err)
            }
            sets = append(sets, spec)
            byt, err := yaml.Marshal(spec)
            fmt.Println(string(byt))
          }
          for _, v := range sets { process(cmd, v) }
        },
}

func init() {
	rootCmd.AddCommand(configCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// configCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
}
