/*
Copyright © 2022 Jan Garcia

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
package cmd

import (
	"os"
	"regexp"
	"strings"

	"github.com/spf13/cobra"

	"github.com/tdewolff/minify/v2"
	"github.com/tdewolff/minify/v2/css"
"github.com/tdewolff/minify/v2/html"
	"github.com/tdewolff/minify/v2/js"

	"compress/gzip"
        "github.com/andybalholm/brotli"
)

var MinifyAll bool = false
var MinifyJS bool = false
var MinifyCSS bool = false
var MinifyHTML bool = false
var MinifyPHP bool = false
var Overwrite bool = false
var CustomExt []string

var ExtHTML = []string{ "html", "htm", }
var ExtJS = []string{ "js", }
var ExtCSS = []string{ "css", }
var ExtPHP = []string{ "php", }

var FlagHTMLKeepConditionalComments bool
var FlagHTMLKeepEndTags bool
var FlagHTMLKeepQuotes bool
var FlagHTMLKeepWhitespace bool

var CompressGzip bool
var CompressBrotli bool

var m = minify.New()

func minfname(path string) string {
  var spl = strings.Split(path, ".")
  return strings.Join(append(spl[:len(spl)-1], "min", spl[len(spl)-1]), ".")
}

func minifylib(path string, mime string) {
  if Overwrite {
    byt, err := os.ReadFile(path)
    if err != nil { panic(err) }
    out, err := m.Bytes(mime, byt)
    if err != nil { panic(err) }
    os.WriteFile(path, out, 0666)
  } else {
    inf, err := os.Open(path)
    if err != nil { panic(err) }
    outf, err := os.Create(minfname(path))
    if err != nil { panic(err) }
    err = m.Minify(mime, outf, inf)
    if err != nil { panic(err) }
    inf.Close()
    outf.Close()
  }
}

func Contains[T comparable](s []T, e T) bool {
  for _, v := range s {
    if v == e {
      return true
    }
  }
  return false
}

var php_remove_single_comments = regexp.MustCompile("//.*[\n\r]")
var php_remove_multi_comments = regexp.MustCompile("/\\*.*([\n\r].*)*\\*/")
var php_remove_newlines = regexp.MustCompile("[\n\r][ \t]*")
func php_minifier(src string) string {
  return php_remove_newlines.ReplaceAllString(php_remove_multi_comments.ReplaceAllString(php_remove_single_comments.ReplaceAllString(src, ""), ""), " ")
}

func minifyjs(path string) { minifylib(path, "application/javascript") }
func minifycss(path string) { minifylib(path, "text/css") }
func minifyhtml(path string) { minifylib(path, "text/html") }
func minifyphp(path string) {
  byt, err := os.ReadFile(path)
  if err != nil { panic(err) }
  out := []byte(php_minifier(string(byt)))
  if err != nil { panic(err) }
  if Overwrite {
    os.WriteFile(path, out, 0666)
  } else {
    os.WriteFile(minfname(path), out, 0666)
  }
}

func GetOutputFilename(path string) string {
  if Overwrite {
    return path
  }
  return minfname(path)
}

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "prodgomin <input files>",
	Short: "Production minifier written in Go",
	Long: `Prodgomin is a production minifier written in Go, it's goal is to create a minified version of your codebase directly in your production environment.`,
        Args: cobra.ArbitraryArgs,
        Version: "v1.0.0",
	// Uncomment the following line if your bare application
	// has an action associated with it:
	Run: func(cmd *cobra.Command, args []string) {
          for _, v := range CustomExt {
            var spl = strings.Split(v, ":")
            if spl[1] == "html" { ExtHTML = append(ExtHTML, spl[0]) }
            if spl[1] == "css" { ExtCSS = append(ExtCSS, spl[0]) }
            if spl[1] == "js" { ExtJS = append(ExtJS, spl[0]) }
            if spl[1] == "php" { ExtPHP = append(ExtPHP, spl[0]) }
          }

          m.AddFunc("text/css", css.Minify)
          m.Add("text/html", &html.Minifier{
            KeepDocumentTags: true,
            KeepConditionalComments: FlagHTMLKeepConditionalComments,
            KeepEndTags: FlagHTMLKeepEndTags,
            KeepQuotes: FlagHTMLKeepQuotes,
            KeepWhitespace: FlagHTMLKeepWhitespace,
          })
          m.AddFuncRegexp(regexp.MustCompile("^(application|text)/(x-)?(java|ecma)script$"), js.Minify)

          if MinifyAll {
            MinifyJS = true
            MinifyCSS = true
            MinifyHTML = true
            MinifyPHP = true
          }
          for _, v := range args {
            if strings.Contains(v, ".min.") { continue }
            var spl = strings.Split(v, ".")
            
            var compress bool = true
            if Contains(ExtJS, spl[len(spl) - 1]) && MinifyJS {
              minifyjs(v)
            } else if Contains(ExtCSS, spl[len(spl) - 1]) && MinifyCSS {
              minifycss(v)
            } else if Contains(ExtHTML, spl[len(spl) - 1]) && MinifyHTML {
              minifyhtml(v)
            } else if Contains(ExtPHP, spl[len(spl) - 1]) && MinifyPHP {
              minifyphp(v)
            } else {
              compress = false
            }

            if compress && CompressGzip {
              f, err := os.Create(GetOutputFilename(v) + ".gz")
              if err != nil { panic(err) }
              byt, err := os.ReadFile(GetOutputFilename(v))
              if err != nil { panic(err) }
              w := gzip.NewWriter(f)
              w.Write(byt)
              w.Close()
              f.Close()
            }
            if compress && CompressBrotli {
              f, err := os.Create(GetOutputFilename(v) + ".br")
              if err != nil { panic(err) }
              byt, err := os.ReadFile(GetOutputFilename(v))
              if err != nil { panic(err) }
              w := brotli.NewWriter(f)
              w.Write(byt)
              w.Close()
              f.Close()
            }
          }
          if len(args) == 0 {
            cmd.Help()
          }
        },
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	err := rootCmd.Execute()
	if err != nil {
		os.Exit(1)
	}
}

func RootCommandInit() {
  rootCmd.Flags().BoolVarP(&MinifyAll, "all", "a", false, "Minifies ALL source files")
  rootCmd.Flags().BoolVar(&MinifyJS, "js", false, "Minifies JS source files")
  rootCmd.Flags().BoolVar(&MinifyCSS, "css", false, "Minifies CSS source files")
  rootCmd.Flags().BoolVar(&MinifyHTML, "html", false, "Minifies HTML source files")
  rootCmd.Flags().BoolVar(&MinifyPHP, "php", false, "Minifies PHP source files")
  rootCmd.Flags().BoolVarP(&Overwrite, "overwrite", "o", false, "Overwrites source files instead of creating .min versions")
  rootCmd.Flags().StringSliceVarP(&CustomExt, "extension", "e", []string{}, "Defines a new custom extension for supported formats, 'ext:format'")

  rootCmd.Flags().BoolVar(&FlagHTMLKeepConditionalComments, "html-KeepConditionalComments", false, "Flag for the HTML minifier")
  rootCmd.Flags().BoolVar(&FlagHTMLKeepEndTags, "html-KeepEndTags", false, "Flag for the HTML minifier")
  rootCmd.Flags().BoolVar(&FlagHTMLKeepQuotes, "html-KeepQuotes", false, "Flag for the HTML minifier")
  rootCmd.Flags().BoolVar(&FlagHTMLKeepWhitespace, "html-KeepWhitespace", false, "Flag for the HTML minifier")

  rootCmd.Flags().BoolVar(&CompressGzip, "gz", false, "Compresses the output files with GZip")
  rootCmd.Flags().BoolVar(&CompressBrotli,"br", false, "Compresses the output files with Brotli")
}

func init() {
  RootCommandInit()
}


